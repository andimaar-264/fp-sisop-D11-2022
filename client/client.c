#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <dirent.h>
#include <ctype.h>

struct userAllowed {
	char name[10001];
	char password[10001];
};

int cekAllowed(char *username, char *password) {
	FILE *fp;
	struct allowed user;
	int id,found = 0;
	fp = fopen("/");  // Lokasi file

	while(true) {	
		fread(&user, sizeof(user), 1, fp);
		if(strcmp(user.name, username) == 0){
			if(strcmp(user.password, password) == 0)
				found = 1;
		}
		if(feof(fp))
			break;
	}

	fclose(fp);
	if(found == 0) {
		printf("Login Rejected.\n");
		return 0;
	} else
		return 1;
}

int main(int argc, char *argv[]) {
    int allowed = 0;
	int id_user = geteuid();
	char database_used[1001];

	if(geteuid() == 0)
		allowed = 1;
	else {
		int id = geteuid();
		allowed = cekAllowed(argv[2], argv[4]);
	}
	if(allowed == 0)
		return 0;

	int clientSocket, ret;
	struct sockaddr_in serverAddr;
	char buffer[32000];

	clientSocket = socket(AF_INET, SOCK_STREAM, 0);
	if(clientSocket < 0) {
		printf("Connection failed.\n");
		exit(1);
	}
	printf("Client Socket created.\n");

	memset(&serverAddr, '\0', sizeof(serverAddr));
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(4443);
	serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");

	ret = connect(clientSocket, (struct sockaddr*)&serverAddr, sizeof(serverAddr));
	if(ret < 0) {
		printf("Connection error.\n");
		exit(1);
	}
	printf("Connected...\n");
	while(true) {
		printf("Client: \t");
		char input[10001];
		char command[101][10001];
		char *token;
		int i = 0;

		scanf(" %[^\n]s", input);
		token = strtok(input, " ");
		while( token != NULL ) {
			strcpy(command[i], token);
			i++;
			token = strtok(NULL, " ");
		}

        if(strcmp(command[0], "CREATE") == 0) {
            if(strcmp(command[1], "USER") == 0 && strcmp(command[3], "IDENTIFIED") == 0 && strcmp(command[4], "BY") ==  0) {
                snprintf(buffer, sizeof buffer, "cUser:%s:%s:%d", command[2], command[5], id_user);
                send(clientSocket, buffer, strlen(buffer), 0);
            }
        }

		if(strcmp(command[0], ":exit") == 0) {
			close(clientSocket);
			printf("Disconnected.\n");
			exit(1);
		}

		if(recv(clientSocket, buffer, 1024, 0) < 0)
			printf("Failed to receive data.\n");
		else
			printf("Server: \t%s\n", buffer);
	}
	return 0;
}
