#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>

struct userAllowed {
	char name[10001];
	char password[10001];
};

void createUser(char *name, char *password){
	struct userAllowed user;
	strcpy(user.name, name);
	strcpy(user.password, password);
	printf("%s %s\n", user.name, user.password);
	char fileLoc[] = { "/user.dat" }; // Lokasi file
	FILE *fp;
	fp = fopen(fileLoc,"ab");
	fwrite(&user, sizeof(user), 1, fp);
	fclose(fp);
}

int main() {
	int sockfd, ret;
	struct sockaddr_in serverAddr;
	int newSocket;
	struct sockaddr_in newAddr;

	socklen_t addr_size;

	char buffer[1024];
	pid_t childpid;

	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if(sockfd < 0) {
		printf("Connection failed.\n");
		exit(1);
	}
	printf("Server Socket created.\n");

	memset(&serverAddr, '\0', sizeof(serverAddr));
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(4443);
	serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");

	ret = bind(sockfd, (struct sockaddr*)&serverAddr, sizeof(serverAddr));
	if(ret < 0) {
		printf("Binding error.\n");
		exit(1);
	}
	printf("Bind to port %d\n", 4444);

	if(listen(sockfd, 10) == 0)
		printf("Listening to port %d...\n", 444);
	else
		printf("Binding error.\n");
	
	while(1) {
		newSocket = accept(sockfd, (struct sockaddr*)&newAddr, &addr_size);
		if(newSocket < 0)
			exit(1);
		printf("Connection from %s:%d\n", inet_ntoa(newAddr.sin_addr), ntohs(newAddr.sin_port));

		if((childpid = fork()) == 0) {
			close(sockfd);
			while(1) {
				recv(newSocket, buffer, 1024, 0);
                char *token;
				char buffercopy[32001];
				strcpy(buffercopy, buffer);
				char command[101][10001];
				token = strtok(buffercopy, ":");
				int i = 0;
				char database_used[1001];
				while(token != NULL) {
					strcpy(command[i], token);
					i++;
					token = strtok(NULL, ":");
				}

                if(strcmp(command[0], "cUser") == 0){
                    if(strcmp(command[3], "0") == 0){
                        createUser(command[1], command[2]);
                    }else{
                        char alert[] = "Login rejected.";
                        send(newSocket, alert, strlen(alert), 0);
                        bzero(buffer, sizeof(buffer));
                    }
                }

				if(strcmp(buffer, ":exit") == 0) {
					printf("Disconnected from %s:%d\n", inet_ntoa(newAddr.sin_addr), ntohs(newAddr.sin_port));
					break;
				} else {
					printf("Client: %s\n", buffer);
					send(newSocket, buffer, strlen(buffer), 0);
					bzero(buffer, sizeof(buffer));
				}
			}
		}

	}
	close(newSocket);
	return 0;
}
